/**
 * @jest-environment jsdom
 */
import React from 'react'
import { render, within, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import { ShardCalculator } from './shard'

test('should default to 0 shards', () => {
    render(<ShardCalculator />);
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('0');
})

test('should show correct max shards after clicking the max all button', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    await user.click(screen.getByText('Max All Gates'))
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('770');
})

test('should show correct shards after clicking the clear all button', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    await user.click(screen.getByText('Max All Gates'))
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('770');

    await user.click(screen.getByText('Clear All Gates'))
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('0');
})

test('should show correct max shards after setting the config to 2/0', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    await user.selectOptions(screen.getByLabelText('1'), '2')
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('15');
})

test('should show correct max shards after setting the config to 2/5/2/2/5/0', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    await user.selectOptions(screen.getByLabelText('1'), '2')
    await user.selectOptions(screen.getByLabelText('2'), '5')
    await user.selectOptions(screen.getByLabelText('3'), '2')
    await user.selectOptions(screen.getByLabelText('4'), '2')
    await user.selectOptions(screen.getByLabelText('5'), '5')
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('245');
})

test('should show correct max shards after setting the config to 2/5/2/2/5/0 and then changing the 2nd gate to 0', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    await user.selectOptions(screen.getByLabelText('1'), '2')
    await user.selectOptions(screen.getByLabelText('2'), '5')
    await user.selectOptions(screen.getByLabelText('3'), '2')
    await user.selectOptions(screen.getByLabelText('4'), '2')
    await user.selectOptions(screen.getByLabelText('5'), '5')
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('245');
    
    await user.selectOptions(screen.getByLabelText('2'), '0')
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('15');
})

test('should correctly set previous gates to min steps when changing a later gate', async () => {
    const user = userEvent.setup();
    render(<ShardCalculator />);

    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('0');
    
    await user.selectOptions(screen.getByLabelText('5'), '5');
    
    expect(screen.getByTestId('total-shards-display')).toHaveTextContent('160');
    
    expect(within(screen.getByLabelText('1')).getByText('2').selected).toBe(true);
    expect(within(screen.getByLabelText('2')).getByText('2').selected).toBe(true);
    expect(within(screen.getByLabelText('3')).getByText('2').selected).toBe(true);
    expect(within(screen.getByLabelText('4')).getByText('2').selected).toBe(true);
    expect(within(screen.getByLabelText('5')).getByText('5').selected).toBe(true);
    expect(within(screen.getByLabelText('6')).getByText('0').selected).toBe(true);
    expect(within(screen.getByLabelText('7')).getByText('0').selected).toBe(true);
})

