import React from 'react';
import { createRoot } from "react-dom/client";
import { ShardCalculator } from './shard';

const root = createRoot(document.getElementById('shard_container'));
root.render(<ShardCalculator />);
